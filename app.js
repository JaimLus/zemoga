'use strict'

//Configuracion de Express
const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');


//Ruteros de la aplicacion
const routerURL = require("./routes/index");
const routerBackendVotes = require("./controller/backend/backend.votes");

//Creacion de aplicacion
const app = express();

// Configuracion de la aplicacion
app.engine("html", require("ejs").renderFile);
app.set("views", path.join(__dirname, "views"));

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

//Rutas estaticas
app.use("/",express.static("./public"));
app.use("/",express.static("./controller"));
app.use("/",express.static("./views"));

//Definicion de cabeceras
app.use(function(req,res,next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, sid");
	res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
	next();
})

//Usabilidad de rutas
app.use("/", routerURL);
app.use("/", routerBackendVotes);

module.exports = app;