var mongo = require("mongoose");
var schema = mongo.Schema;

var schemaVotes = new schema({
  idVotes: {
    type: Number
  },
  votesTrue: {
    type: Number
  },
  votesFalse: {
    type: Number
  },
  totalVotes: {
    type: Number
  }
});

var modelVotes = mongo.model("votes", schemaVotes);
//model es el constructor

module.exports = modelVotes;