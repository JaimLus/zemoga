var express = require("express");
var modelVotes = require("../../models/model.votes");
var path = require('path');
var routerVotes = express.Router();

routerVotes.route("/votes")
  	.get(function(req, res) {
  		var filtro = {};

    	modelVotes.find(filtro)
    	.then((votes) =>{
         	res.send(votes);
     	})
     	.catch((err) =>{
       		message: "Error al tratar de traer los registros"
     	});
  	})
  	.put(function(req,res){

		let voteId = req.body._id;
		let body = req.body;

		modelVotes.findByIdAndUpdate(voteId, body)
		.then((votes) =>{
			res.status(200).send({
				votes: votes
			});
		})
		.catch((err)=>{
			res.status(500).send(error);
		})
	});
  	
	

module.exports = routerVotes;