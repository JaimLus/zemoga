var appZemoga = angular.module('AppZemoga', ["ngRoute"]);

//Definicion del controlador para votaciones
appZemoga.controller('controllerVotes', ['$scope', '$http', '$route', function(scope, http, route){
	//votes, es el guarado de la informacion proveniente del DB
	var votes = [];

	//readyData, funcion que recarga los datos en el front
	readyData();

	//localStorage, almacenamiento local (cliente), para el guardado del voto seleccionado
	localStorage.setItem("vote", "");

	//selecVoteTrue, funcion que guarda en local, el voto positivo
	scope.selectVoteTrue = function(data){
		if(data == 1){
			document.querySelector("#c1 img.arriba").classList.add("borderVotesSelect");
			document.querySelector("#c1 img.abajo").classList.remove("borderVotesSelect");
		}
		else if(data == 2){
			document.querySelector("#c2 img.arriba").classList.add("borderVotesSelect");
			document.querySelector("#c2 img.abajo").classList.remove("borderVotesSelect");
		}
		else if(data == 3){
			document.querySelector("#c3 img.arriba").classList.add("borderVotesSelect");
			document.querySelector("#c3 img.abajo").classList.remove("borderVotesSelect");
		}
		else{
			document.querySelector("#c4 img.arriba").classList.add("borderVotesSelect");
			document.querySelector("#c4 img.abajo").classList.remove("borderVotesSelect");
		}
		
		localStorage.setItem("vote", "true");
	}

	//selecVoteFalse, funcion que guarda en local, el voto negativo
	scope.selectVoteFalse = function(data){
		if(data == 1){
			document.querySelector("#c1 img.arriba").classList.remove("borderVotesSelect");
			document.querySelector("#c1 img.abajo").classList.add("borderVotesSelect");
		}
		else if(data == 2){
			document.querySelector("#c2 img.arriba").classList.remove("borderVotesSelect");
			document.querySelector("#c2 img.abajo").classList.add("borderVotesSelect");
		}
		else if(data == 3){
			document.querySelector("#c3 img.arriba").classList.remove("borderVotesSelect");
			document.querySelector("#c3 img.abajo").classList.add("borderVotesSelect");
		}
		else{
			document.querySelector("#c4 img.arriba").classList.remove("borderVotesSelect");
			document.querySelector("#c4 img.abajo").classList.add("borderVotesSelect");
		}

		localStorage.setItem("vote", "false");
	}

	//votes, funcion que almacena el voto seleccionado en DB
	scope.votes = function(data){
		if(localStorage.vote != ""){
			var dataSend = [];
			if(data == 1){
				dataSend = votes[0];
				document.querySelector("#c1 .votes").style.display = "none";
				document.querySelector("#c1 .voteAgain").style.display = "inherit";
				document.querySelector("#c1 img.arriba").classList.remove("borderVotesSelect");
				document.querySelector("#c1 img.abajo").classList.remove("borderVotesSelect");
			}
			else if(data == 2){
				dataSend = votes[1];
				document.querySelector("#c2 .votes").style.display = "none";
				document.querySelector("#c2 .voteAgain").style.display = "inherit";
				document.querySelector("#c2 img.arriba").classList.remove("borderVotesSelect");
				document.querySelector("#c2 img.abajo").classList.remove("borderVotesSelect");
			}
			else if(data == 3){
				dataSend = votes[2];
				document.querySelector("#c3 .votes").style.display = "none";
				document.querySelector("#c3 .voteAgain").style.display = "inherit";
				document.querySelector("#c3 img.arriba").classList.remove("borderVotesSelect");
				document.querySelector("#c3 img.abajo").classList.remove("borderVotesSelect");
			}
			else{
				dataSend = votes[3];
				document.querySelector("#c4 .votes").style.display = "none";
				document.querySelector("#c4 .voteAgain").style.display = "inherit";
				document.querySelector("#c4 img.arriba").classList.remove("borderVotesSelect");
				document.querySelector("#c4 img.abajo").classList.remove("borderVotesSelect");
			}
			
			if(localStorage.vote == "true"){
				dataSend.votesTrue = dataSend.votesTrue + 1;
			}else{
				dataSend.votesFalse = dataSend.votesFalse + 1;
			}
			dataSend.totalVotes = dataSend.votesTrue + dataSend.votesFalse;
			localStorage.setItem("vote", "");

			//Swal, funcion que muestra alerta en pantalla
			swal("Vote Success", {
				buttons: false,
				timer: 2000,
			});

			//http.put, metodo que actualiza los datos en DB
			http.put("/votes",dataSend)
			.then(function(response,status,headers,config){
				readyData();
			})
			.catch(function(response,status){
				console.log('error');
			});
		}
		else{
			swal("Empty vote", {
				buttons: false,
				timer: 2000,
			});
		}
		
	}

	//votesAgain, funcion que muestra la opcion de votar nuevamente
	scope.votesAgain = function(data){
		if(data == 1){
			document.querySelector("#c1 .votes").style.display = "inherit";
			document.querySelector("#c1 .voteAgain").style.display = "none";
		}
		else if(data == 2){
			document.querySelector("#c2 .votes").style.display = "inherit";
			document.querySelector("#c2 .voteAgain").style.display = "none";
		}
		else if(data == 3){
			document.querySelector("#c3 .votes").style.display = "inherit";
			document.querySelector("#c3 .voteAgain").style.display = "none";
		}
		else{
			document.querySelector("#c4 .votes").style.display = "inherit";
			document.querySelector("#c4 .voteAgain").style.display = "none";
		}
	}
	//readyData, funcion que recarga los datos en el front
	function readyData(){
		//http.get(), metodo que obteniene los datos del DB
		http.get("/votes")
			.then(function(response,status,headers,config){
				votes = response.data;
				console.log(votes);
				for (var i =  0; i < response.data.length; i++) {
					if(response.data[i].idVotes == 1){
						var widthTrue = (response.data[i].votesTrue * 100)/(response.data[i].totalVotes);
						var widthFalse = (response.data[i].votesFalse * 100)/(response.data[i].totalVotes);
						document.querySelector('#c1 .trueVotes').style.width = widthTrue+"%";
						document.querySelector('#c1 .trueVotes label').innerHTML  = widthTrue.toFixed(1)+"%";
						document.querySelector('#c1 .falseVotes').style.width = widthFalse+"%";
						document.querySelector('#c1 .falseVotes label').innerHTML  = widthFalse.toFixed(1)+"%";
					}
					else if(response.data[i].idVotes == 2){
						var widthTrue = (response.data[i].votesTrue * 100)/(response.data[i].totalVotes);
						var widthFalse = (response.data[i].votesFalse * 100)/(response.data[i].totalVotes);
						document.querySelector('#c2 .trueVotes').style.width = widthTrue+"%";
						document.querySelector('#c2 .trueVotes label').innerHTML  = widthTrue.toFixed(1)+"%";
						document.querySelector('#c2 .falseVotes').style.width = widthFalse+"%";
						document.querySelector('#c2 .falseVotes label').innerHTML  = widthFalse.toFixed(1)+"%";
					}
					else if(response.data[i].idVotes == 3){
						var widthTrue = (response.data[i].votesTrue * 100)/(response.data[i].totalVotes);
						var widthFalse = (response.data[i].votesFalse * 100)/(response.data[i].totalVotes);
						document.querySelector('#c3 .trueVotes').style.width = widthTrue+"%";
						document.querySelector('#c3 .trueVotes label').innerHTML  = widthTrue.toFixed(1)+"%";
						document.querySelector('#c3 .falseVotes').style.width = widthFalse+"%";
						document.querySelector('#c3 .falseVotes label').innerHTML  = widthFalse.toFixed(1)+"%";
					}
					else{
						var widthTrue = (response.data[i].votesTrue * 100)/(response.data[i].totalVotes);
						var widthFalse = (response.data[i].votesFalse * 100)/(response.data[i].totalVotes);
						document.querySelector('#c4 .trueVotes').style.width = widthTrue+"%";
						document.querySelector('#c4 .trueVotes label').innerHTML  = widthTrue.toFixed(1)+"%";
						document.querySelector('#c4 .falseVotes').style.width = widthFalse+"%";
						document.querySelector('#c4 .falseVotes label').innerHTML  = widthFalse.toFixed(1)+"%";
					}
				}			
			})
			.catch(function(response,status,headers,config){
				console.log("Error")
			});
	}
}]);