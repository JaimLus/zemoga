# Zemoga

Prueba zemoga

En esta rama, encontraran los archivos que usé para el desarrollo de la prueba.

Cabe resaltar que hice esta prueba con el mejor entusiasmo, ya que a medida que iba avanzando en el mismo, me emocionaba bastante.

Tuve inconvenientes de tiempo, los cuales fueron por asuntos personales, por lo cual, no alcancé a hacer la aplicación responsive, ya que pensaba usar bootstrap pero después caí en cuenta que bootstrap hace utilidad de Jquery y no quise que hubiese mal entendidos (una de las restricciones de la prueba era no usar Jquery), entonces, cuando quise implementar el responsive con css puro, ya me había alcanzado la noche de ayer lunes.
Implementé la estructura de la prueba con Node, específicamente con Express, cuando detallen el proyecto se darán cuenta de lo que utilicé para esta solución. Usé de igual manera Angular, Javascript, Html5 y css3.
Tomé la iniciativa de hacer la prueba organizada, por lo cual, guardo los datos en MongoDB, haciendo un respectivo llamado a la DB en mlab (nube).

Para una mejor explicación de lo desarrollado, en el correo enviado (martes 27 de marzo, 01:27 am), adjunté un video donde se muestra el flujo de la aplicación (funcionalidad) y el .zip de la solución.

Espero les guste mi forma de trabajar, les sea de mucho interés, y sé que en este mundo del desarrollo nunca se es experto, ya que siempre tenemos la ansiedad de mejorar. 

Una vez mas, gracias por tenerme en cuenta.