var express = require('express');
var router = express.Router();

/* Principal */
router.get('/', function(req, res, next) {
  res.render('index.html');
});
router.get('/past', function(req, res, next) {
  res.render('past.html');
});
router.get('/how', function(req, res, next) {
  res.render('how.html');
});
router.get('/log', function(req, res, next) {
  res.render('log.html');
});
module.exports = router;